/*
 * Copyright 2020 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <memory>
#include <vector>

namespace func_bench {

namespace virtual_func {

struct IAdder {
    virtual int add(int x, int y) const noexcept = 0;

    void add_vec(const std::vector<int>& left, const std::vector<int>& right,
        std::vector<int>& res) const noexcept;

    virtual ~IAdder() = default;
};

std::shared_ptr<IAdder> create_adder();

}  // namespace virtual_func

}  // namespace func_bench
