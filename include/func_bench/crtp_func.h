/*
 * Copyright 2020 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <memory>
#include <vector>

namespace func_bench {

namespace crtp_func {

template <typename Derived>
class AdderBase {
public:
    const Derived& derived() const noexcept {
        return *static_cast<const Derived*>(this);
    }

    int add(int x, int y) const noexcept { return derived().add(x, y); }

    void add_vec(const std::vector<int>& left, const std::vector<int>& right,
        std::vector<int>& res) const noexcept {
        const std::size_t size = left.size();
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = left[i] + right[i];
        }
    }
};

class Adder : public AdderBase<Adder> {
public:
    int add(int x, int y) const noexcept { return x + y; }
};

}  // namespace crtp_func

}  // namespace func_bench
