/*
 * Copyright 2020 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <benchmark/benchmark.h>

#include <vector>

#define CPP_FUNC_BENCH(func) \
    BENCHMARK(func)->RangeMultiplier(10)->Range(10000, 1000000)

namespace func_bench {

std::vector<int> get_random(std::size_t size);

struct Data {
    std::vector<int> left;
    std::vector<int> right;
    std::vector<int> res;

    explicit Data(std::size_t size)
        : left(func_bench::get_random(size)),
          right(func_bench::get_random(size)),
          res(size) {}
};

}  // namespace func_bench
