Copyright 2020 MusicScience37 (Kenta Kabashima)

This project includes benchmark library, which is available under a 
"Apache License, version 2.0." For details, see extern/benchmark.
