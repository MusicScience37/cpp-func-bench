/*
 * Copyright 2020 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "func_bench/virtual_func.h"

namespace func_bench {

namespace virtual_func {

void IAdder::add_vec(const std::vector<int>& left,
    const std::vector<int>& right, std::vector<int>& res) const noexcept {
    const std::size_t size = left.size();
    for (std::size_t i = 0; i < size; ++i) {
        res[i] = left[i] + right[i];
    }
}

}  // namespace virtual_func

}  // namespace func_bench
