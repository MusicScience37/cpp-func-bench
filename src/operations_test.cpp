/*
 * Copyright 2020 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <benchmark/benchmark.h>

#include <cmath>
#include <cstdint>
#include <random>
#include <vector>

#include "func_bench/common.h"

namespace {

template <typename ValueType>
std::vector<ValueType> get_random_int(std::size_t size) {
    std::mt19937 engine;
    std::uniform_int_distribution<ValueType> dist;

    std::vector<ValueType> vec;
    vec.reserve(size);
    for (std::size_t i = 0; i < size; ++i) {
        vec.push_back(dist(engine));
    }

    return vec;
}

template <typename ValueType>
std::vector<ValueType> get_random_float(std::size_t size) {
    // save the previous state of random number generators
    static std::mt19937 engine;
    std::uniform_real_distribution<ValueType> dist(ValueType(0), ValueType(10));

    std::vector<ValueType> vec;
    vec.reserve(size);
    for (std::size_t i = 0; i < size; ++i) {
        vec.push_back(dist(engine));
    }

    return vec;
}

}  // namespace

static void add_int(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    using ValueType = int;
    using VectorType = std::vector<ValueType>;

    VectorType left = get_random_int<ValueType>(size);
    VectorType right = get_random_int<ValueType>(size);
    VectorType res(size);

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = left[i] + right[i];
        }
    }
}
CPP_FUNC_BENCH(add_int);

static void add_int64(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    using ValueType = std::int64_t;
    using VectorType = std::vector<ValueType>;

    VectorType left = get_random_int<ValueType>(size);
    VectorType right = get_random_int<ValueType>(size);
    VectorType res(size);

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = left[i] + right[i];
        }
    }
}
CPP_FUNC_BENCH(add_int64);

static void add_double(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    using ValueType = double;
    using VectorType = std::vector<ValueType>;

    VectorType left = get_random_float<ValueType>(size);
    VectorType right = get_random_float<ValueType>(size);
    VectorType res(size);

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = left[i] + right[i];
        }
    }
}
CPP_FUNC_BENCH(add_double);

static void mul_double(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    using ValueType = double;
    using VectorType = std::vector<ValueType>;

    VectorType left = get_random_float<ValueType>(size);
    VectorType right = get_random_float<ValueType>(size);
    VectorType res(size);

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = left[i] * right[i];
        }
    }
}
CPP_FUNC_BENCH(mul_double);

static void div_double(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    using ValueType = double;
    using VectorType = std::vector<ValueType>;

    VectorType left = get_random_float<ValueType>(size);
    VectorType right = get_random_float<ValueType>(size);
    VectorType res(size);

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = left[i] / right[i];
        }
    }
}
CPP_FUNC_BENCH(div_double);

static void sqrt_double(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    using ValueType = double;
    using VectorType = std::vector<ValueType>;

    VectorType inp = get_random_float<ValueType>(size);
    VectorType res(size);

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = std::sqrt(inp[i]);
        }
    }
}
CPP_FUNC_BENCH(sqrt_double);

static void exp_double(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    using ValueType = double;
    using VectorType = std::vector<ValueType>;

    VectorType inp = get_random_float<ValueType>(size);
    VectorType res(size);

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = std::exp(inp[i]);
        }
    }
}
CPP_FUNC_BENCH(exp_double);

static void atan2_double(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    using ValueType = double;
    using VectorType = std::vector<ValueType>;

    VectorType x = get_random_float<ValueType>(size);
    VectorType y = get_random_float<ValueType>(size);
    VectorType res(size);

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            res[i] = std::atan2(y[i], x[i]);
        }
    }
}
CPP_FUNC_BENCH(atan2_double);
