/*
 * Copyright 2020 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <benchmark/benchmark.h>

#include <vector>

#include "func_bench/common.h"

static void lambda_func(benchmark::State &state) {
    const std::size_t size = static_cast<std::size_t>(state.range());
    func_bench::Data data(size);

    auto add = [](int x, int y) { return x + y; };

    for (auto _ : state) {
        for (std::size_t i = 0; i < size; ++i) {
            data.res[i] = add(data.left[i], data.right[i]);
        }
    }
}
CPP_FUNC_BENCH(lambda_func);
