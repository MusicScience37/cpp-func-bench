/*
 * Copyright 2020 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "func_bench/common.h"

#include <random>

namespace func_bench {

std::vector<int> get_random(std::size_t size) {
    std::mt19937 engine;
    std::uniform_int_distribution<int> dist;

    std::vector<int> vec;
    vec.reserve(size);
    for (std::size_t i = 0; i < size; ++i) {
        vec.push_back(dist(engine));
    }

    return vec;
}

}  // namespace func_bench
